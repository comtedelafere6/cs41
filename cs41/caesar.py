def encrypt_caesar(z, k):
    """
    Encrypts plaintext using a Caesar cipher.

    >>> encrypt_caesar("PYTHON", 3)
    'SBWKRQ'
    >>> encrypt_caesar("python", 3)
    'sbwkrq'
    >>> encrypt_caesar("", 3)
    ''
    """
    c = ''
    for i in z:
        if ((ord(i) >= 65) and (ord(i) <= 90)) or ((ord(i) >= 97) and (ord(i) <= 122)):
            if z.islower():
                m = ord(i) - ord('a')
                m = (m + k) % 26 + ord('a')
                i = chr(m)
                c = c + i
            if z.isupper():
                m = ord(i) - ord('A')
                m = (m + k) % 26 + ord('A')
                i = chr(m)
                c = c + i
        else:
            c = c + i
    ciphertext = c
    return ciphertext


def decrypt_caesar(z, k):
    """
    Decrypts a ciphertext using a Caesar cipher.

    >>> decrypt_caesar("SBWKRQ", 3)
    'PYTHON'
    >>> decrypt_caesar("sbwkrq", 3)
    'python'
    >>> decrypt_caesar("", 3)
    ''
    """
    c = ''
    for i in z:
        if ((ord(i) >= 65) and (ord(i) <= 90)) or ((ord(i) >= 97) and (ord(i) <= 122)):
            if z.islower():
                m = ord(i) - ord('a')
                m = (m - k) % 26 + ord('a')
                i = chr(m)
                c = c + i
            if z.isupper():
                m = ord(i) - ord('A')
                m = (m - k) % 26 + ord('A')
                i = chr(m)
                c = c + i
        else:
            c = c + i
    plaintext = c
    return plaintext
