def encrypt_vigenere(plaintext, keyword):
    """
    >>> encrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> encrypt_vigenere("python", "a")
    'python'
    >>> encrypt_vigenere("ATTACKATDAWN", "LEMON")
    'LXFOPVEFRNHR'
    """
    keyword = keyword * (((len(plaintext) - len(keyword)) // len(keyword)) + 2)
    b = []
    lenght = len(plaintext)
    alphabet1 = 'abcdefghijklmnopqrstuvwxyz'
    alphabet2 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for a in keyword:
        if keyword.islower():
            x = alphabet1.find(a)
            b.append(x)
        if keyword.isupper():
            x = alphabet2.find(a)
            b.append(x)
    c = ''
    k = 0
    v = 0
    for i in plaintext:
        if plaintext.islower():
            k = v
            m = ord(i) - ord('a')
            m = (m + b[k]) % 26 + ord('a')
            i = chr(m)
            i.lower()
            c = c + i
            v = v + 1
        if plaintext.isupper():
            k = v
            m = ord(i) - ord('A')
            m = (m + b[k]) % 26 + ord('A')
            i = chr(m)
            i.upper()
            c = c + i
            v = v + 1
    ciphertext = c
    return ciphertext


def decrypt_vigenere(ciphertext, keyword):
    """
    >>> decrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> decrypt_vigenere("python", "a")
    'python'
    >>> decrypt_vigenere("LXFOPVEFRNHR", "LEMON")
    'ATTACKATDAWN'
    """
    keyword = keyword * (((len(ciphertext) - len(keyword)) // len(keyword)) + 2)
    b = []
    lenght = len(ciphertext)
    alphabet1 = 'abcdefghijklmnopqrstuvwxyz'
    alphabet2 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for a in keyword:
        if keyword.islower():
            x = alphabet1.find(a)
            b.append(x)
        if keyword.isupper():
            x = alphabet2.find(a)
            b.append(x)
    c = ''
    k = 0
    v = 0
    for i in ciphertext:
        if ciphertext.islower():
            k = v
            m = ord(i) - ord('a')
            m = (m - b[k]) % 26 + ord('a')
            i = chr(m)
            i.lower()
            c = c + i
            v = v + 1
        if ciphertext.isupper():
            k = v
            m = ord(i) - ord('A')
            m = (m - b[k]) % 26 + ord('A')
            i = chr(m)
            i.upper()
            c = c + i
            v = v + 1
    plaintext = c
    return plaintext