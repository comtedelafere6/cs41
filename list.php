<?php

include 'connection.php';

$query = "SELECT driver.experience, set_off.defect_reason AS defect FROM 
		set_off JOIN (driver JOIN bus ON driver.passport = bus.passport) 
		ON set_off.registration_number = bus.registration_number";

$result = mysqli_query($link, $query);

echo "<table border = 1 align=center> <tr> <td> Стаж водителя </td> <td> Поломки автобуса </td></tr>";

while($row = mysqli_fetch_array($result)) {
	echo "<tr><td>" . $row['experience']. "</td><td>" . $row['defect'] . "</td></tr>";
}

echo "</table>";

mysqli_close($link);