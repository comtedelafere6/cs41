<?php

include 'connection.php';

$query ="create table if not exists driver (
passport int(11) NOT NULL,
class text NOT NULL,
experience int(11) NOT NULL,
salary int(11) NOT NULL,
age int(11) NOT NULL,
PRIMARY KEY (passport)
)";

$result = mysqli_query($link, $query) or die("Error " . mysqli_error($link)); 
if($result)
{
echo "<p>Well-done";
}

$sql = "INSERT INTO driver (class, experience, salary, age, passport) VALUES
('1', '3', '20000', '18', '123'),
('2', '1', '15000', '17', '456'),
('1', '20', '30000', '58', '789')";
if (mysqli_query($link, $sql)) {
echo "<p>Created successfully<br>";
} else {
echo "<p>Error creating <br>" . mysqli_error($link);
}

$query ="create table if not exists bus (
registration_number int(11) NOT NULL,
type text NOT NULL,
capacity int(11) NOT NULL,
passport int(11) NOT NULL,
number_of_buses int(11),
PRIMARY KEY (registration_number),
FOREIGN KEY (passport) REFERENCES driver (passport)
)";

$result = mysqli_query($link, $query) or die("Error " . mysqli_error($link)); 
if($result)
{
echo "<p>Well-done";
}

$sql = "INSERT INTO bus (registration_number, type, capacity, passport) VALUES
('1', 'x', '20', '123'),
('2', 'y', '15', '456'),
('3', 'z', '30', '789')";

if (mysqli_query($link, $sql)) {
echo "<p>Created successfully<br>";
} else {
echo "<p>Error creating <br>" . mysqli_error($link);
}

$query ="create table if not exists route(
route_number int(11) NOT NULL,
departure_point text NOT NULL,
arrival_point text NOT NULL,
departure_time int(11) NOT NULL,
arrival_time int(11) NOT NULL,
arrival_interval int(11) NOT NULL,
extent int(11) NOT NULL,
PRIMARY KEY (route_number)
)";

$result = mysqli_query($link, $query) or die("Error " . mysqli_error($link)); 
if($result)
{
echo "<p>Well-done";
}

$sql = "INSERT INTO route (route_number, departure_point, arrival_point, departure_time, arrival_time, arrival_interval, extent) VALUES
('5', 'Dno', 'San Francisco', '8', '17', '9', '60000'),
('11', 'Ploshad Vosstaniya', 'Novgorodskaya', '10', '20', '10', '5'),
('22', 'Moscow', 'Petushki', '9', '22', '13', '78')";

if (mysqli_query($link, $sql)) {
echo "<p>Created successfully<br>";
} else {
echo "<p>Error creating <br>" . mysqli_error($link);
}

$query ="create table if not exists set_off (
id_set_off int(11) NOT NULL,
defect_reason text NOT NULL,
route_number int(11) NOT NULL,
date int(11) NOT NULL,
registration_number int(11) NOT NULL,
passport int(11) NOT NULL,
PRIMARY KEY (id_set_off),
FOREIGN KEY (route_number) REFERENCES route (route_number),
FOREIGN KEY (registration_number) REFERENCES bus (registration_number),
FOREIGN KEY (passport) REFERENCES driver (passport)
)";

$result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link)); 
if($result)
{
echo "<p>Well-done";
}

$sql = "INSERT INTO set_off (route_number, date, registration_number, defect_reason, id_set_off, passport) VALUES
('22', '2', '1', '-', '1', '123'),
('5', '27', '2', '-', '2', '456'),
('11', '25', '3', 'tyre', '3', '789')";
if (mysqli_query($link, $sql)) {
echo "<p>Created successfully<br>";
} else {
echo "<p>Error creating <br>" . mysqli_error($link);
}

$query ="create table if not exists schedule (
id int(11) NOT NULL,
responsible text NOT NULL,
route_number int(11) NOT NULL,
registration_number int(11) NOT NULL,
passport int(11) NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (route_number) REFERENCES route (route_number),
FOREIGN KEY (registration_number) REFERENCES bus (registration_number),
FOREIGN KEY (passport) REFERENCES driver (passport)
)";

$result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link)); 
if($result)
{
echo "<p>Well-done";
}

$sql = "INSERT INTO schedule (responsible, id, registration_number, route_number, passport) VALUES
('Sidorov', '1', '1', '22', '123'),
('Petrov', '2', '2', '5', '456'),
('Kuznetsov', '3', '3', '11', '789')";
if (mysqli_query($link, $sql)) {
echo "<p>Created successfully<br>";
} else {
echo "<p>Error creating <br>" . mysqli_error($link);
}

$query ="create table if not exists defect (
defect_reason varchar(10) NOT NULL,
PRIMARY KEY (defect_reason)
)";

$result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link)); 
if($result)
{
echo "<p>Well-done";
}

$sql = "INSERT INTO defect (defect_reason) VALUES
('motor'),
('tyre')";
if (mysqli_query($link, $sql)) {
echo "<p>Created successfully<br>";
} else {
echo "<p>Error creating <br>" . mysqli_error($link);
}

mysqli_close($link);
?>